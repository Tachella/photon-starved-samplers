function  [z_mean,z_var,Nmc] = RWM(y,x_init,Nrow,run_time,lambda,delta,BurnIn,plot_debug)
%% RWM

x = x_init;
mean_z = zeros(size(y));
squared_z = zeros(size(y));
flag = false;
Nbi = Inf;

opt_acc = 0.23;
    
%% first iter
expx = exp(x); Px = apply_P(x,lambda);
aux = forw_op(expx);
Ux = sum(aux)-y'*log(aux)+x'*Px/2;
    
accepted = 0;
i=1;
tic;
while(toc < run_time)
    %% RWM step
    x_prop = x + sqrt(delta)*randn(size(x));
    
    expx = exp(x_prop); Px = apply_P(x_prop,lambda);
    aux = forw_op(expx);
    Ux_prop = sum(aux)-y'*log(aux)+x_prop'*Px/2;
    
    acc = exp(Ux-Ux_prop);
    if acc > rand
        x = x_prop;
        Ux = Ux_prop;
        if acc>1
            acc = 1;
        end
    end
    
%   lambda = sample_lambda(x);

   
   accepted = accepted + acc;
   if toc/run_time>BurnIn
        if ~flag 
            flag = true;
            Nbi = i;
           disp(['delta: ' num2str(delta) ])
           disp(['accepted: ' num2str(accepted/i*100) ' %' ])
        end
        expx = exp(x);
        mean_z = mean_z + expx;
        squared_z = squared_z + expx.^2;
   else
       delta = adapt_stepsize(delta,acc,i,opt_acc); 
   end
   
    %% plot stuff
    if plot_debug && rem(i,1e4)==0
        disp(['completed: ' num2str(toc/run_time*100) ' %'])
        disp(['delta: ' num2str(delta) ])
        disp(['accepted: ' num2str(accepted/i*100) ' %' ])
        figure(1)
        if i<Nbi
            imagesc(reshape(exp(x),Nrow,Nrow))
        else
            imagesc(reshape(mean_z/(i-Nbi),Nrow,Nrow))
        end
        axis image
        if i>Nbi
            figure(2)
            imagesc(reshape(squared_z/(i-Nbi)-(mean_z/(i-Nbi)).^2,Nrow,Nrow))
            axis image
            colormap;
        end
        pause(0.1)
    end
   
    i = i +1;
end

%%

disp(['Nmc : ' num2str(i) ' MCMC iterations ']);
z_mean = mean_z/(i-Nbi);
Nmc = i;
z_var = squared_z/(i-Nbi)-(mean_z/(i-Nbi)).^2;

end