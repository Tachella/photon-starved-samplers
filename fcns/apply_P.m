function y=apply_P(x,lambda)

Nrow = sqrt(length(x));
x = reshape(x,Nrow,Nrow);

h = fspecial('laplacian',0);
h = conv2(h,h);
%  
h = h*lambda;
y = imfilter(x, h,'same','circular');

% y = lambda*(x-medfilt2(x));

%y = lambda*(x-FastNonLocalMeans3D(x, 1));
y = y(:);

%y = zeros(size(x));

end