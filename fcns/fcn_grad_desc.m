function x = fcn_grad_desc(y,lambda)

max_iter = 1e3;

x = log(y+1);

thresh = 0.1;
mu = 0;

%% gradient descent
g = @(x) exp(x+mu).*(1-forw_op(y./forw_op(exp(x+mu))))+apply_P(x,lambda);

tol = 1e-2;  maxit = 100;
%%
for i=1:max_iter
    grad = g(x);    
    
    gg = grad'*grad;
    if gg/length(x)<thresh
        break
    end
    
%% Hessian update
    c = forw_op(exp(x+mu));
    
    Q_fun = @(x) (apply_P(x,lambda)+c.*x); 
    x_delta = pcg(Q_fun,grad,tol,maxit);
    
    x = x - x_delta;
end


end