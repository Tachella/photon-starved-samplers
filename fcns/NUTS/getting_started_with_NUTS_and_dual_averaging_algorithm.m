
%% Adjust stepsize via dual averaging.
photon_mean = 1;
fig = 10;
%[y,z_true,Nrow] = get_data(photon_mean,fig);
Nrow = 10;
z_true = exp(0*ones(Nrow));
z_true = z_true(:);
y = poissrnd(forw_op(z_true));
x_init = log(z_true);
NMSE_INIT = quality(z_true,exp(x_init));  
lambda = 2;

f = @UgradU;

%%

seed = 1;
rng(seed)
n_warmup = 100;
opt_acc = .8; 

theta0 = x_init;
nargs = 1;

% Run NUTS and adapt its step-size using dual-averaging algorithm.
[theta, epsilon, epsilon_seq, epsilonbar_seq] = dualAveraging(f, y, lambda, theta0, opt_acc, n_warmup, nargs);

set(0,'defaultAxesFontSize', 18) 
plot(epsilon_seq)
hold on
plot(epsilonbar_seq)
title('Stepsize adaptation via dual-averaging')
xlabel('Iteration')
ylabel('Stepsize')
legend('Attempted value at each iteration', 'Running (weighted) average')
hold off

%% Run NUTS with a fixed stepsize to generate posterior samples.
n_mcmc = 500;
n_updates = 10;
n_itr_per_update = ceil(n_mcmc / n_updates);
samples = zeros(length(theta), n_mcmc);
logp_samples = zeros(n_mcmc, 1);

[samples(:,1), ~, nfevals_total, logp_samples(1)] = NUTS(f, y, lambda, epsilon, theta);
for i = 2:n_mcmc
    [samples(:,i), ~, nfevals, logp_samples(i)] = NUTS(f, y, lambda, epsilon, samples(:,i-1));
    nfevals_total = nfevals_total + nfevals;
    if mod(i, n_itr_per_update) == 0
        fprintf('%d iterations have been completed.\n', i);
    end
end
fprintf('Each iteration of NUTS required %.1f gradient evaluations on average.\n', ...
    nfevals_total / n_mcmc);

% Basic convergence diagnostic.
set(0,'defaultAxesFontSize', 18) 
plot(logp_samples)
xlabel('Iterations')
ylabel('Log probability')
title('Traceplot of $\log(\pi(\theta))$', 'Interpreter', 'LaTex')





%% Compute effective sample sizes (ESS) for each parameter. 
% The estimator used here is one of the most reliable and provide estimates
% that are generally in the right ballpark (as long as the length of a
% chain is much longer than the lag it takes for the auto-correlation to
% become negligible).
% 
% set(0,'defaultAxesFontSize', 18) 
% marker_size = 6;
% ess_mean = ESS(samples);
% ess_sec_moment = ESS(samples.^2); 
% plot(ess_mean, 'o', 'MarkerSize', marker_size)
% hold on
% plot(ess_sec_moment, 'x', 'MarkerSize', marker_size)
% refline(0, size(samples, 2))
% legend('for mean', 'for 2nd moment')
% xlabel('Parameters')
% ylabel('ESS')
% title('Effective sample sizes')
% hold off
