function  [U, grad_U] = UgradU(x,y,lambda)


expx = exp(x);
Px = apply_P(x,lambda);
grad_U = expx.*(1 - forw_op(y./(forw_op(expx))) ) + Px; 
aux = forw_op(expx);
U = sum(aux)-y'*log(aux)+x'*Px/2;

U = -U;
grad_U = -grad_U;

end