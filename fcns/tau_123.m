function tau=tau_123(y,x,v,lambda)


max_iter = 100;
iter_error = 1e-5;

%% for debug
% t = (0:0.001:2)';
% g1 = zeros(length(t),1);
% g2 = zeros(length(t),1);
% for i = 1:length(t)
%     aux = forw_op(exp(x+v*t(i)));
%     g1(i) = sum(aux);
%     g2(i) = -y'*log(aux);
% end
%  plot(t,(g1))
%  hold on
% plot(t,(g2))
% gtot = g1+g2;
%  plot(t,gtot - gtot(1));
% keyboard


Pv = apply_P(v,lambda);
Px = apply_P(x,lambda);

vPv = v'*Pv;
xPv = x'*Pv;
xPx = x'*Px;

%% find tau_ast = argmin U(x+v*t)

iter = 0;
tau_old = Inf; 
tau_ast = 0;

while iter < max_iter && abs(tau_old-tau_ast) > iter_error

    expx = exp(x+v*tau_ast);
    vexpx = v.*expx;
    vvexpx = v.*vexpx;
    aux_1 = forw_op(vexpx);
    aux_2 = forw_op(expx);
    aux_3 = forw_op(vvexpx);
    g_prime = sum(vexpx) + (-y'*(aux_1./aux_2)) + (tau_ast*vPv+xPv);
    g_prime2 = sum(vvexpx) + y'*( (aux_1./aux_2).^2 - aux_3./aux_2)+ (vPv);

    tau_old = tau_ast;
    tau_ast = tau_ast - g_prime/g_prime2;
    iter = iter +1;
end

% disp('tau ast')
% iter

%% find tau | U(x+v*tau)-U(x+v*tau_asterisk) = -logV

    

if tau_ast<0 
    tau_ast = 0;
end

expx = exp(x+v*tau_ast);
aux_2 = forw_op(expx);
g = sum(expx)-y'*log(aux_2) + tau_ast^2*vPv/2 + tau_ast*xPv + xPx/2;
logV = log(rand);
C = logV - g;

iter = 0;
tau_old = Inf; 
tau = tau_ast*1.05+1e-4;

while iter < max_iter && abs(tau_old-tau) > iter_error

    expx = exp(x+v*tau);
    vexpx = v.*expx;
    aux_1 = forw_op(vexpx);
    aux_2 = forw_op(expx);
    g = sum(expx) -y'*log(aux_2) + tau^2*vPv/2 + tau*xPv + xPx/2;
    g_prime = sum(vexpx) + (-y'*(aux_1./aux_2)) + tau*vPv + xPv ;

    tau_old = tau;
    tau = tau - (g+C)/(g_prime+1e-5);
    iter = iter +1;
end
% disp('tau')
%     iter


if isnan(tau) || tau<tau_ast
    keyboard
end


end