function [z_mean,z_var,bounces] = BPS(y,x_init,Nrow,run_time,lambda,lambda_ref,BurnIn,plot_debug)

%%  Non Local BPS

%% Algorithm Parameters
k = 1; %timestep indicator
t_total = 0;


%% Initialization of x and v
x = x_init; % hot start
v = randn(size(x));
mean_z = zeros(size(x)); 
var_z = zeros(size(x)); 
random_bounces = 0;

% mean_debug = zeros(1e5,1);
% var_debug = zeros(1e5,1);
% var2_debug = zeros(1e5,1);
% positions = zeros(1e5,1);

%% Main loop
tic;
while(toc<run_time)
    k=k+1;
    %% sample tau_bonce
    %[tau_bounce,index] = min([tau_2(y,x,v);tau_3(x,v,lambda);tau_1(x,v)]);
    tau_bounce = tau_123(y,x,v,lambda);
    
    
    %% sample tau_ref
    tau_ref = -log(rand)/lambda_ref;
    
    %% find trayectory length
    [tau,ind]=min([tau_ref,tau_bounce]);
    
    %tau
    
    %% compute posterior statistics
    
    if toc>BurnIn*run_time
        mean_z = mean_z +(exp(x+v*tau)-exp(x))./v;
        var_z = var_z +(exp(2*x+2*v*tau)-exp(2*x))./(2*v);
        %% update clock
        t_total = t_total+tau;
    end
     
%     var2_debug(k) = (exp(2*x(1)+2*v(1)*tau)-exp(2*x(1)))./(2*v(1))/tau - ((exp(x(1)+v(1)*tau)-exp(x(1)))./v(1)/tau)^2  ;
     
    x=x+v*tau;
    if ind==2 
       %bounce    
       expx = exp(x);
       delta_U = expx.*(1 - forw_op(y./(forw_op(expx))) ) + apply_P(x,lambda);
       
       %elastic collision
       v = v-2*(v'*delta_U)/(delta_U'*delta_U)*delta_U;
    else
       %refresh
       random_bounces = random_bounces + 1;
       v = randn(size(v));
    end
    
    
    
     
     
    %% debug 
%     positions(k) = exp(x(1));
%     mean_debug(k) = mean_z(1)/t_total;
%     var_debug(k) = var_z(1)/t_total -  (mean_z(1)/t_total)^2;
    
    %% plot stuff
    if plot_debug && rem(k,400)==0
        disp(['Completed: ' num2str(toc/run_time*100) ' %'])
        figure(1)
        imagesc(reshape(mean_z/t_total,Nrow,Nrow))
        axis image
        axis off
        figure(2)
        imagesc(reshape(var_z/t_total-(mean_z/t_total).^2,Nrow,Nrow))
        axis image
        axis off
        disp(['random bounces: ' num2str(random_bounces/k*100) ' %'])
        figure(3)
        plot(mean_debug(1:k))
        hold on
        plot(var_debug(1:k))
        plot(positions(1:k))
        plot(var2_debug(1:k))
        hold off
        pause(0.1)
    end
end


%%

disp(['k : ' num2str(k) ' bounces ']);
disp(['total fict. sim. time : ' num2str(t_total)]);
z_mean = mean_z/t_total;
z_var = var_z/t_total-(mean_z/t_total).^2;
bounces = k;
