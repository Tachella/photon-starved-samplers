function y=forw_op(x)

%y = x(:);
h = fspecial('average',7);
%h = 1;

Nrow = sqrt(length(x));
x = reshape(x,Nrow,Nrow);

y = imfilter(x, h,'same','replicate');

y = y(:);

end