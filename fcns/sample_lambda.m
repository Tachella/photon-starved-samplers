function [lambda,Px,xPx]=sample_lambda(x)
alpha = 1;
beta = 1;

Px = apply_P(x,1);
xPx = x'*Px/2;
lambda = gamrnd(alpha+(length(x)-2)/2,1/(beta+xPx));

%% for Laplacian reg. rank(D) = length(x) - 2.  See Gaussian Markov Random Fields (havard rue and leonard held)

end