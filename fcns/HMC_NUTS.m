function [z_mean,z_var,Nmc] = HMC_NUTS(y,x_init,Nrow,run_time,lambda,BurnIn,plot_debug)

f = @UgradU;

opt_acc = .6; 

theta0 = x_init;
nargs = 1;

% Run NUTS and adapt its step-size using dual-averaging algorithm.
tic;
[theta, epsilon, Nbi] = dualAveraging(f, y, lambda, theta0, opt_acc, BurnIn*run_time, nargs);


%% Run NUTS with a fixed stepsize to generate posterior samples.

z_mean = zeros(size(theta));
squared_z = z_mean;

n_itr_per_update = 100;

accepted = 0;

[x_samples, acc, nfevals_total, ~] = NUTS(f, y, lambda, epsilon, theta);
accepted = accepted + acc;
i = 2;
while(toc<run_time)
    [x_samples, acc, nfevals, ~] = NUTS(f, y, lambda, epsilon, x_samples);
    accepted = accepted + acc;
    nfevals_total = nfevals_total + nfevals;
    
    z_mean = z_mean + exp(x_samples);
    squared_z = squared_z + exp(2*x_samples);
    
    if plot_debug && mod(i, n_itr_per_update) == 0
        fprintf('%d iterations have been completed.\n', i);
        disp(['acceptance rate: ' num2str(accepted/i)])
        figure(1)
        imagesc(reshape(z_mean/i,Nrow,Nrow))
        axis image
        axis off
        figure(2)
        imagesc(reshape(squared_z/i-(z_mean/i).^2,Nrow,Nrow))
        axis image
        axis off
        pause(0.1)
    end
    i=i+1;
end

fprintf('Each iteration of NUTS required %.1f gradient evaluations on average.\n', ...
    nfevals_total / i);

disp([num2str(i) ' NUTS HMC MCMC iterations'])
Nmc = i + Nbi;

z_mean = z_mean/i;
z_var = squared_z/i-z_mean.^2;

end
