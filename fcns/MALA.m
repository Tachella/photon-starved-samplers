function [z_mean,z_var,Nmc] = MALA(y,x_init,Nrow,run_time,lambda,BurnIn,plot_debug)
%% MALA

accepted = 0;
%bound = 1e5;

x = x_init;
mean_z = zeros(size(y));
squared_z = zeros(size(y));
% x_debug = zeros(1,1e4);

thinning_factor = 1;

%% initial epsilon
epsilon = initial_epsilon(x,y,lambda);
delta = epsilon^2;

%% first ULA step
expx = exp(x);
Px = apply_P(x,lambda);
grad_U = expx.*(1 - forw_op(y./(forw_op(expx))) ) + Px; 
% norm_deltaU = sqrt(sum(delta_U.^2));
% if norm_deltaU > bound % truncated MALA
%    delta_U = delta_U/norm_deltaU;
% end
old_prop_mean = x - delta/2*grad_U;
aux = forw_op(expx);
Ux = sum(aux)-y'*log(aux)+x'*Px/2;

opt_acc = 0.57;
samples = 0;
i=1;
tic;
while(toc<run_time)
    %% ULA step
   x_prop = old_prop_mean + sqrt(delta)*randn(size(x));
   
   %% MALA step
   expx = exp(x_prop);
   Px = apply_P(x_prop,lambda);
   aux = forw_op(expx);
   grad_U = expx.*(1 - forw_op(y./aux) ) + Px; 
   
%    norm_deltaU = sqrt(sum(delta_U.^2));
%    if norm_deltaU > bound % truncated MALA
%        delta_U = delta_U/norm_deltaU;
%    end
   new_prop_mean = x_prop - delta/2*grad_U;
   Uxprop = sum(aux)-y'*log(aux)+x_prop'*Px/2;
   
   acc = exp(Ux - Uxprop - ( sum((new_prop_mean-x).^2) - sum((x_prop-old_prop_mean).^2))/(2*delta)); 
   
    if acc>rand
      x = x_prop; 
      old_prop_mean = new_prop_mean;
      Ux = Uxprop;
      if acc>1 
          acc = 1;
      end
    end
    
    accepted = accepted + acc;
   
   %% adapt stepsize 
    if toc/run_time < BurnIn
        old_delta = delta;
        delta = adapt_stepsize(delta,acc,i,opt_acc);
        old_prop_mean = x+(old_prop_mean-x)/old_delta*delta;
    end
    
    if samples==1
        disp(['accepted: ' num2str(accepted/i*100) ' %'])
        disp(['step size: ' num2str(delta)])
    end
    
%%    Sample lambda
%     old_lambda = lambda;
%     [lambda,Px,xPx] = sample_lambda(x);
%     old_prop_mean = old_prop_mean + delta/2*(old_lambda-lambda)*Px;
%     Ux = Ux - (old_lambda-lambda)*xPx;
   

%%   Posterior statistics
   if toc/run_time > BurnIn
       
       if rem(i,thinning_factor)==0
        samples = samples + 1;
        expx = exp(x);
        mean_z = mean_z + expx;
        squared_z = squared_z + expx.^2;
       end
   end
   
%    x_debug(1,i) = x(1);
  
        
   
    %% plot stuff
    if plot_debug && samples > 0 && rem(samples,5000) == 0
        disp(['accepted: ' num2str(accepted/i*100) ' %'])
        disp(['step size: ' num2str(delta)])
        disp(['completed: ' num2str(toc/run_time*100) ' %'])
        disp(['lambda: ' num2str(lambda) ])
        figure(1)
        imagesc(reshape(mean_z/samples,Nrow,Nrow))
        axis image
        axis off
        figure(2)
        imagesc((reshape(squared_z/samples-(mean_z/samples).^2,Nrow,Nrow)))
        axis image
        axis off
%         figure(3)
%         plot(x_debug(1,1:i))
%         hold on
%         plot(cumsum(exp(2*x_debug(1,1:i))-exp(x_debug(1,1:i)))./(1:i))
%         hold off
        pause(0.1)
    end
   i = i + 1;
end

disp(['Nmc : ' num2str(i) ' MCMC iterations ']);
disp([num2str(samples) ' samples ']);
z_mean = mean_z/samples;
Nmc= i;
z_var = squared_z/samples-(mean_z/samples).^2;


