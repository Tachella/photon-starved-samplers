function tau=tau_2(y,x,v)

tau = 0;

max_iter = 100;
iter_error = 1e-10;

%% for debug
% t = (0:0.1:10)';
% g1 = zeros(length(t),1);
% g2 = zeros(length(t),1);
% for i = 1:length(t)
%     aux = forw_op(exp(x+v*t(i)));
%     g1(i) = sum(aux);
%     g2(i) = -y'*log(aux);
% end
% % plot(t,(g1))
% % hold on
% plot(t,(g2))
% % plot(t,(g1+g2));
% keyboard

%% 
aux_1 = forw_op(v.*exp(x));
aux_2 = forw_op(exp(x));
g_prime = (-y'*(aux_1./aux_2));

if g_prime<0 
    tau = +Inf;
else
%% find tau | U(x+v*tau)-U(x+v*tau_asterisk) = -logV
    iter = 0;
    tau_old = Inf; 

    aux_2 = forw_op(exp(x));
    g = -y'*log(aux_2);
    logV =log(rand);
    C = logV - g;

    while iter < max_iter && abs(tau_old-tau) > iter_error

        aux_1 = forw_op(v.*exp(x+v*tau));
        aux_2 = forw_op(exp(x+v*tau));
        g = -y'*log(aux_2);
        g_prime = (-y'*(aux_1./aux_2));

        tau_old = tau;
        tau = tau - (g+C)/g_prime;
        iter = iter +1;
    end
    %iter
end

% if v'*y<0
%     tau = logV/(v'*y);
% else
%     tau=Inf;
% end


end