function delta = adapt_stepsize(delta,acc,iter,opt)

delta = min([1e1,max([1e-8, delta - (opt-acc)/iter/10])]);

end