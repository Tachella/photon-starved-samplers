function tau = tau_3(x,v,lambda)
 

Pv = apply_P(v,lambda);

xPv = x'*Pv;
vPv = v'*Pv;

tau = (-xPv+sqrt((xPv>0)*xPv^2-vPv*log(rand)))/vPv;


end