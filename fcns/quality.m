function NMSE = quality(x_true,x_est)

if size(x_true,2)>1
    x_true = x_true(:);
end

if size(x_est,2)>1
    x_est = x_est(:);
end

NMSE = sum((x_true-x_est).^2) / sum(x_true.^2);

disp(['NMSE: ' num2str(NMSE) ])
end