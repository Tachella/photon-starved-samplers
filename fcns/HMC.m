function [z_mean,z_var,Nmc] = HMC(y,x_init,Nrow,run_time,lambda,epsilonL,BurnIn,plot_debug)
%% HMC

%% algo params
x=x_init;
mean_z = zeros(size(y));
squared_z = zeros(size(y));

accepted = 0;

%% initial epsilon
epsilon = initial_epsilon(x,y,lambda);
L = 10;
epsilon = epsilon/L;
 
H_bar = 0;
epsilon_bar = 1;
Nbi = Inf;
mu = log(10*epsilon);

flag = false;

% x_debug = zeros(1,2000);


expx = exp(x);
Px = apply_P(x,lambda);
grad_U = expx.*(1 - forw_op(y./(forw_op(expx))) ) + Px; 
aux = forw_op(expx);
U = sum(aux)-y'*log(aux)+x'*Px/2;

opt_acc = 0.65;


i=1;
tic;
while(toc<run_time)
    %% HMC step
   [x,acc,grad_U,U] = HMC_step(y,x,lambda,epsilon,grad_U,U,L);
  
   
   %% adapt epsilon
   if toc/run_time < BurnIn
%           delta = adapt_stepsize(epsilon^2,acc,i,opt_acc);
%           epsilon = sqrt(delta);
          [epsilon,epsilon_bar,H_bar] = adapt_epsilonHMC(i,H_bar,epsilon_bar,mu,acc,opt_acc);
          L = round(epsilonL/epsilon);
   end
   
   %% sample regularization parameter
   %lambda = sample_lambda(x);
   
   accepted = accepted + acc;
   if toc/run_time>BurnIn
       if ~flag
           epsilon = exp(epsilon_bar);
           flag = true;
           Nbi = i;
            disp(['accepted: ' num2str(accepted/i*100) ' %'])
            disp(['lambda: ' num2str(lambda)])
            disp(['L: ' num2str(L)])
       end
       expx = exp(x);
       mean_z = mean_z + expx;
       squared_z = squared_z + expx.^2;
   end
   
%     x_debug(1,i)=x(1);
   
    %% plot stuff
    if plot_debug && rem(i,2000) == 0
        disp(['completed: ' num2str(toc/run_time*100) ' %'])
        disp(['accepted: ' num2str(accepted/i*100) ' %'])
        disp(['lambda: ' num2str(lambda)])
        disp(['L: ' num2str(L)])
        figure(1)
        if i<Nbi
            imagesc(reshape(exp(x),Nrow,Nrow))
        else
            imagesc(reshape(mean_z/(i-Nbi),Nrow,Nrow))
        end
        axis image
        if i>Nbi
            figure(2)
            imagesc(reshape(squared_z/(i-Nbi)-(mean_z/(i-Nbi)).^2,Nrow,Nrow))
            axis image
            colormap;
        end
        figure(3)
        plot(x_debug(1,1:i))
        hold on
        plot(cumsum(exp(2*x_debug(1,1:i))-exp(x_debug(1,1:i)))./(1:i))
        hold off
        figure(4)
        histogram(x_debug(1,1:i))
        pause(0.1)
    end
   i=i+1;
end

z_mean = mean_z/(i-Nbi);
z_var = squared_z/(i-Nbi)-(mean_z/(i-Nbi)).^2;
Nmc = i;
disp(['Nmc : ' num2str(i) ' MCMC iterations ']);




