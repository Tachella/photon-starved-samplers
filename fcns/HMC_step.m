function [x,acc,last_grad_U,last_U] = HMC_step(y,x,lambda,epsilon,grad_U,U,L)

%% parameters
%path_length = ceil(5*1e-5);
%L = min([20,path_length/epsilon]);


%% algorithm
q = x;

last_grad_U = grad_U;
last_U = U;

p = randn(size(x)); % independent standard normal variates
current_p = p;


% Make a half step for momentum at the beginning
current_U = U;

p = p - epsilon * grad_U / 2;
% Alternate full steps for position and momentum
for i= 1:L
    % Make a full step for the position
    q = q + epsilon * p;
    
    % Make a full step for the momentum, except at end of trajectory
    if i~=L
       expx = exp(q);
       Px = apply_P(q,lambda);
       grad_U = expx.*(1 - forw_op(y./forw_op(expx)) ) + Px; 
       p = p - epsilon * grad_U;
   
    end
    
end

% Make a half step for momentum at the end.
expx = exp(q);
Px = apply_P(q,lambda);
aux = forw_op(expx);
proposed_U = sum(aux)-y'*log(aux)+q'*Px/2;

grad_U = expx.*(1 - forw_op(y./aux) ) + Px; 
p = p - epsilon * grad_U / 2;
   

    
%%  Evaluate potential and kinetic energies at start and end of trajectory
current_K = sum(current_p.^2) / 2;
proposed_K = sum(p.^2) / 2;

acc = exp(current_U-proposed_U+current_K-proposed_K);
if acc>rand
    x = q;
    last_grad_U = grad_U;
    last_U = proposed_U;
    if acc > 1
        acc = 1;
    end
end

end