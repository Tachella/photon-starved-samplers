function [z_mean,z_var,Nmc] = ULA(y,x_init,Nrow,run_time,lambda,delta,BurnIn,plot_debug)
%% ULA

sqrt_delta = sqrt(delta);

x = x_init;
mean_z = zeros(size(y));
squared_z = zeros(size(y));
flag = false;
Nbi = Inf;

i=1;
tic;
while (toc < run_time)
    %% ULA step
    if toc/run_time<=BurnIn
        expx = exp(x);
    end
    Px = apply_P(x,lambda);
    delta_U = expx.*(1 - forw_op(y./(forw_op(expx))) ) + Px; 
    x = x - delta/2*delta_U + sqrt_delta*randn(size(x));
   
%   lambda = sample_lambda(x);
   
   if toc/run_time>BurnIn
        if ~flag 
            flag = true;
            Nbi = i;
        end
        expx = exp(x);
        mean_z = mean_z + expx;
        squared_z = squared_z + expx.^2;
   end
   
    %% plot stuff
    if plot_debug && rem(i,2e3)==0
        disp(['completed: ' num2str(toc/run_time*100) ' %'])
        disp(['lambda: ' num2str(lambda) ])
        figure(1)
        if i<Nbi
            imagesc(reshape(exp(x),Nrow,Nrow))
        else
            imagesc(reshape(mean_z/(i-Nbi),Nrow,Nrow))
        end
        axis image
        if i>Nbi
            figure(2)
            imagesc(reshape(squared_z/(i-Nbi)-(mean_z/(i-Nbi)).^2,Nrow,Nrow))
            axis image
            colormap;
        end
        pause(0.1)
    end
   
    i = i +1;
end

%%

disp(['Nmc : ' num2str(i) ' MCMC iterations ']);
z_mean= mean_z/(i-Nbi);
Nmc = i;
z_var = squared_z/(i-Nbi)-(mean_z/(i-Nbi)).^2;

end

