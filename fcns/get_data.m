function [y,z_true,Nrow]=get_data(lambda_mean,fig,Nrow)

z_true = im2double(imread('cameraman.tif'));
z_true = imresize(z_true,[Nrow,Nrow]);

z_true = z_true(:);
z_true = z_true/mean(z_true)*lambda_mean;
y = poissrnd(forw_op(z_true));
y(isnan(y))=1;

figure(fig)
subplot(121)
imagesc(reshape(z_true,Nrow,Nrow))
axis image
axis off
colormap('jet')
colorbar

subplot(122)
imagesc(reshape(y,Nrow,Nrow))
axis image
axis off
colormap('jet')
colorbar



