function epsilon=initial_epsilon(q,y,lambda)
epsilon = 1e-2;

p = randn(size(q));

[q_prop,p_prop] = LeapFrog(q,p,epsilon,y,lambda);

expx = exp(q);
Px = apply_P(q,lambda);
aux = forw_op(expx);
U = sum(aux)-y'*log(aux)+q'*Px/2;

expx = exp(q_prop);
Px = apply_P(q_prop,lambda);
aux = forw_op(expx);
U_prop = sum(aux)-y'*log(aux)+q'*Px/2;

acc = min([1,exp(U-U_prop+p'*p/2-p_prop'*p_prop/2)]);

a = 2*(acc>0.5)-1;

while exp(a*(U-U_prop+p'*p/2-p_prop'*p_prop/2))>2^(-a)
   epsilon = epsilon*2^a;
    [q_prop,p_prop] = LeapFrog(q,p,epsilon,y,lambda);
    expx = exp(q);
    Px = apply_P(q,lambda);
    aux = forw_op(expx);
    U = sum(aux)-y'*log(aux)+q'*Px/2;
    expx = exp(q_prop);
    Px = apply_P(q_prop,lambda);
    aux = forw_op(expx);
    U_prop = sum(aux)-y'*log(aux)+q'*Px/2;
end

end