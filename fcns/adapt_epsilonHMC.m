function [epsilon,epsilon_bar,H_bar] = adapt_epsilonHMC(m,H_bar,epsilon_bar,mu,accep_prob,opt_acc)

t0 = 10;
k = 0.75;
gamma = 0.05;

%% primal dual averaging, Nesterov (2009)

H_bar = (1-1/(m+t0))*H_bar+1/(m+t0)*(opt_acc-accep_prob);

epsilon = mu - sqrt(m)/gamma*H_bar;

epsilon_bar = m^(-k)*epsilon + (1-m^(-k))*epsilon_bar;

epsilon = exp(epsilon);

end