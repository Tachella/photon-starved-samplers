function tau=tau_1(x,v)

%% for debug
% t = (0:0.1:10)';
% g1 = zeros(length(t),1);
% g2 = zeros(length(t),1);
% for i = 1:length(t)
%     aux = forw_op(exp(x+v*t(i)));
%     g1(i) = sum(aux);
%     g2(i) = -y'*log(aux);
% end
%  plot(t,(g1))
% % hold on
% %plot(t,(g2))
% % plot(t,(g1+g2));
% keyboard

%% this assumes sum(A,2) = 1;
logV = log(rand(size(x)));
tau = (-x+log(exp(x)-logV))./v;

tau(tau<0)=Inf;


end