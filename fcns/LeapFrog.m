function [q,p] = LeapFrog(q,p,epsilon,y,lambda)


%% half step
expx = exp(q);
Px = apply_P(q,lambda);
aux = forw_op(expx);
grad_U = expx.*(1 - forw_op(y./aux) ) + Px; 

p = p - epsilon * grad_U / 2;
%% full step
q = q + epsilon * p;
   
%%  Make a half step for momentum at the end.
expx = exp(q);
Px = apply_P(q,lambda);
aux = forw_op(expx);
grad_U = expx.*(1 - forw_op(y./aux) ) + Px; 

p = p - epsilon * grad_U / 2;

end

