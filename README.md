# photon-starved-samplers

MATLAB Codes of "Bayesian Restoration Of High-Dimensional Photon-Starved
Images", implemented by J. Tachella.

Authors: J. Tachella, Y. Altmann, M. Pereyra, J-.Y. Tourneret and S. McLaughlin

Contact: jat3@hw.ac.uk, https://tachella.github.io/eusipco2018/

Please run the script 'example.m' and see the details therein.

These codes include all the state-of-the-art MCMC samplers (Random Walk Metropolis, Hamiltonian Monte Carlo, No U-turn HMC, Bouncy particle sampler, Metropolis Adjusted Langevin Algorithm and Metropolis Unadjusted Langevin Algorithm for the Poisson deconvolution problem.