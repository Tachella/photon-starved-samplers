clear all
close all
clc
%% 
% MATLAB Codes of "Bayesian Restoration Of High-Dimensional Photon-Starved
% Images", implemented by J. Tachella.
% Authors: J. Tachella, Y. Altmann, M. Pereyra, J-.Y. Tourneret and S. McLaughlin
% Contact: jat3@hw.ac.uk, https://tachella.github.io/eusipco2018/
% This script runs the MCMC samplers (Random Walk Metropolis, Hamiltonian
% Monte Carlo, No U-turn HMC, Bouncy particle sampler, Metropolis Adjusted
% Langevin Algorithm and Metropolis Unadjusted Langevin Algorithm for the
% Poisson deconvolution problem.
% The forward operator can be easily modified by changing the function
% 'forw_op'
% The Laplacian regularization can be also modified by changing the
% function 'apply_P'

%% Add functions to MATLAB path
addpath(genpath('fcns'));

%% Parameters
run_time = 200; % total running time per sampler in secs
lambda = 1; % regularization parameter for Laplacian reg.

photon_mean = 1; % mean photons per pixel
Nrow = 50; % Number of rows 
plot_debug = false; % plot intermediate samples during the execution of the samplers
BurnIn = 0.3; % Proportion of MCMC samples to be discarded (Burn-in iterations)
fig = 10;

%% Initialization
[y,z_true,Nrow] = get_data(photon_mean,fig,Nrow); % get cameraman image
pause(2)

x_init = fcn_grad_desc(y,lambda);
NMSE_INIT = quality(z_true,exp(x_init));

%% RWM
disp('running RWM: ')
delta = 0.1;
[z_mean,z_var] = RWM(y,x_init,Nrow,run_time,lambda,delta,BurnIn,plot_debug);
NMSE_RWM = quality(z_true,z_mean);
figure(12)
subplot(236)
imagesc(reshape(z_mean,Nrow,Nrow))
axis image 
axis off
title('RWM mean')
figure(13)
subplot(236)
imagesc(reshape(z_var,Nrow,Nrow))
axis image 
axis off
title('RWM var')

%% HMC
disp('running HMC: ')
epsilonL = 0.01;
[z_mean,z_var] = HMC(y,x_init,Nrow,run_time,lambda,epsilonL,BurnIn,plot_debug);
NMSE_HMC = quality(z_true,z_mean);
figure(12)
subplot(232)
imagesc(reshape(z_mean,Nrow,Nrow))
axis image 
axis off
title('HMC mean')
figure(13)
subplot(232)
imagesc(reshape(z_var,Nrow,Nrow))
axis image 
axis off
title('HMC var')

%% NUTS HMC
disp('running NUTS HMC: ')
[z_mean,z_var] = HMC_NUTS(y,x_init,Nrow,run_time,lambda,BurnIn,plot_debug);
NMSE_NUTS_HMC = quality(z_true,z_mean);

figure(12)
subplot(231)
imagesc(reshape(z_mean,Nrow,Nrow))
axis image
axis off
title('NUTS HMC mean')
figure(13)
subplot(231)
imagesc(reshape(z_var,Nrow,Nrow))
axis image
axis off
title('NUTS HMC var')


%% BPS
disp('running BPS: ')
lambda_ref = 2e0; %refreshment rate
[z_mean,z_var] = BPS(y,x_init,Nrow,run_time,lambda,lambda_ref,BurnIn,plot_debug);
NMSE_BPS = quality(z_true,z_mean);
figure(12)
subplot(235)
imagesc(reshape(z_mean,Nrow,Nrow))
axis image 
axis off
title('BPS mean')
figure(13)
subplot(235)
imagesc(reshape(z_var,Nrow,Nrow))
axis image 
axis off
title('BPS var')

%% MALA
disp('running MALA: ')
[z_mean,z_var] = MALA(y,x_init,Nrow,run_time,lambda,BurnIn,plot_debug);
NMSE_MALA = quality(z_true,z_mean);

figure(12)
subplot(233)
imagesc(reshape(z_mean,Nrow,Nrow))
axis image 
axis off
title('MALA mean')
figure(13)
subplot(233)
imagesc(reshape(z_var,Nrow,Nrow))
axis image 
axis off
title('MALA var')

%% ULA
disp('running ULA: ')
delta = 1e-3;
[z_mean,z_var] = ULA(y,x_init,Nrow,run_time,lambda,delta,BurnIn);
NMSE_ULA = quality(z_true,z_mean);
figure(12)
subplot(234)
imagesc(reshape(z_mean,Nrow,Nrow))
axis image 
axis off
title('ULA mean')
figure(13)
subplot(234)
imagesc(reshape(z_var,Nrow,Nrow))
axis image 
axis off
title('ULA var')


